Debloat Windows

Many people are using Windows VM's for specific tasks in Linux rather than a bare metal install of Windows- which is a good thing. 

Some employers require the use of Microsoft Products, and sometimes there are proprietary programs that don't exist in the Linux world that have to be deployed.

In these less than ideal situations you can still remove a tremendous amount of Windows telemetry and malware. This will improve performance as well as improve privacy. You will lose Windows "Features," but it doesn't really matter because this is for specific use cases. It doesn't make sense to run Windows as a daily driver in a VM anyway.

When looking at the contents below, if there is something you want to preserve, just don't copy it over or comment it out. 

You will need to open an Admin Command Line Window and an Admin Power Shell Window. You can make the following into scripts if you want, but the fastest way is to cut and paste them into the approp. window and hit enter.

NOTE: If your are making a script out in PowerShell enter the following to enable scripts:

Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
